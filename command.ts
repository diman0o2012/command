/* Общий для всех команд интерфейс, состоит из одного метода для запуска команды */
 interface Command {
    execute(): void;
}
/*--------------------------------------------------------------------------------*/

/* Конкретные команды, которые передают вызов получателям */
 class Command1 implements Command {
    private str: string;
    private receiver: Receiver_hello;

    constructor(str: string, receiver: Receiver_hello) {
        this.receiver=receiver;
        this.str=str;
    }

    public execute(): void {
        console.log("Команда 1: Поприветствовать");
        this.receiver.doSomething(this.str);
    }
}

 class Command2 implements Command {
    private a: number;
    private b: number;
    private receiver1: Receiver_sum;
    private receiver2: Receiver_mul;

    constructor(a: number, b: number, receiver1: Receiver_sum, receiver2: Receiver_mul) {
        this.a=a;
        this.b=b;
        this.receiver1=receiver1;
        this.receiver2=receiver2;
    }

    public execute(): void {
        console.log("Команда 2: Получить сумму и произведение чисел a и b");
        this.receiver1.doSomething(this.a, this.b);
        this.receiver2.doSomething(this.a, this.b);
    }
}
/*-----------------------------------------------------------------------------------*/


/* Получатели содержат бизнес-логику программы */
class Receiver_hello {
    public doSomething(str: string): void {
        console.log("Получатель 1:", str);
    }
}

class Receiver_sum {
    public doSomething(a: number, b:number): void {
        console.log("Получатель 2: a + b = ", a+b);
    }
}

class Receiver_mul {
    public doSomething(a: number, b:number): void {
        console.log("Получатель 3: a * b = ", a*b);
    }
}
/*-----------------------------------------------------------------------------------*/


/* Отправитель хранит ссылку на объект команды и обращается к нему, 
когда нужно выполнить какое-то действие */
 class Invoker {
    private taskOne: Command;
    private taskTwo: Command;

    public doTaskOne(command: Command): void {
        this.taskOne=command;
    }
    public doTaskTwo(command: Command): void {
        this.taskTwo=command;
    }

    public mainTask(): void {
        console.log('Отправитель: Сделать первое задание');
        if (this.isCommand(this.taskOne)) {
            this.taskOne.execute();
        }
        console.log('Отправитель: Сделать второе задание');
        if (this.isCommand(this.taskTwo)) {
            this.taskTwo.execute();
        }
    }

    private isCommand(object): object is Command {
        return object.execute!==undefined;
    }
}
/*---------------------------------------------------------------------------------*/


/* Клиент создаёт объекты конкретных команд, передавая в них все необходимые параметры */
const invoker = new Invoker();
const receiver_hell = new Receiver_hello();
const receiver_sum = new Receiver_sum();
const receiver_mul = new Receiver_mul();

invoker.doTaskOne(new Command1('Hello world!', receiver_hell));
invoker.doTaskTwo(new Command2(456, 23, receiver_sum, receiver_mul));
invoker.mainTask();