/*--------------------------------------------------------------------------------*/
/* Конкретные команды, которые передают вызов получателям */
class Command1 {
    constructor(str, receiver) {
        this.receiver = receiver;
        this.str = str;
    }
    execute() {
        console.log("Команда 1: Поприветствовать");
        this.receiver.doSomething(this.str);
    }
}
class Command2 {
    constructor(a, b, receiver1, receiver2) {
        this.a = a;
        this.b = b;
        this.receiver1 = receiver1;
        this.receiver2 = receiver2;
    }
    execute() {
        console.log("Команда 2: Получить сумму и произведение чисел a и b");
        this.receiver1.doSomething(this.a, this.b);
        this.receiver2.doSomething(this.a, this.b);
    }
}
/*-----------------------------------------------------------------------------------*/
/* Получатели содержат бизнес-логику программы */
class Receiver_hello {
    doSomething(str) {
        console.log("Получатель 1:", str);
    }
}
class Receiver_sum {
    doSomething(a, b) {
        console.log("Получатель 2: a + b = ", a + b);
    }
}
class Receiver_mul {
    doSomething(a, b) {
        console.log("Получатель 3: a * b = ", a * b);
    }
}
/*-----------------------------------------------------------------------------------*/
/* Отправитель хранит ссылку на объект команды и обращается к нему,
когда нужно выполнить какое-то действие */
class Invoker {
    doTaskOne(command) {
        this.taskOne = command;
    }
    doTaskTwo(command) {
        this.taskTwo = command;
    }
    mainTask() {
        console.log('Отправитель: Сделать первое задание');
        if (this.isCommand(this.taskOne)) {
            this.taskOne.execute();
        }
        console.log('Отправитель: Сделать второе задание');
        if (this.isCommand(this.taskTwo)) {
            this.taskTwo.execute();
        }
    }
    isCommand(object) {
        return object.execute !== undefined;
    }
}
/*---------------------------------------------------------------------------------*/
/* Клиент создаёт объекты конкретных команд, передавая в них все необходимые параметры */
const invoker = new Invoker();
const receiver_hell = new Receiver_hello();
const receiver_sum = new Receiver_sum();
const receiver_mul = new Receiver_mul();
invoker.doTaskOne(new Command1('Hello world!', receiver_hell));
invoker.doTaskTwo(new Command2(456, 23, receiver_sum, receiver_mul));
invoker.mainTask();
